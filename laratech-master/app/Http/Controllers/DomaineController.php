<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;

use App\Domaine;
use Illuminate\Http\Request;
use App\Question;
use App\Reponse;
use Illuminate\Support\Facades\Auth;


class DomaineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $domaines = domaine::paginate(10);
        return view ('Domaines.index')->with('domaines' ,$domaines);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {  if(!Auth::check()) {
         $domaines = domaine::paginate(10);

         return redirect('domaines')->with('error', 'Access denied')->with('domaines',$domaines);
    }
        return view('domaines.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'description' => 'required'
        ]);
        $domaine = new domaine;
        $domaine->nom = $request->input('nom');
        $domaine->description = $request->input('description');

        $domaine->save();
        $domaines = domaine::paginate(10);
        return view ('domaines.index')->with('domaines' ,$domaines)->with('success', 'domaine added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $domaine = Domaine::find($id);
        return view('domaines.show',compact('domaine'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $domaine = domaine::find($id);

        if (!isset($domaine )){
            return view('domaines')->with('error', 'domaine not found');
        }
        if(!Auth::check()){

            return redirect('domaines')->with('error', 'Access denied');
        }
        return view('domaines.edit',compact('domaine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required',
            'description' => 'required'
        ]);
        $domaine = domaine::find($id);
        $domaine->nom = $request->input('nom');
        $domaine->description = $request->input('description');

        $domaine->save();

        return view('domaines')->with('success', 'domaine Modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function destroy($id)
    {
        $Domaine= Domaine::find($id);

        if(!Auth::check()){
            return redirect('domaines')->with('error', 'Unauthorized page');

        }


        $Domaine->delete();
        return view('domaines')->with('success', 'domaine Supprimé');
    }




}
