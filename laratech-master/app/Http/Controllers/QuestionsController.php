<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\chapitre;
use Illuminate\Support\Facades\Auth;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $questions   = Question::paginate(10);
        return view ('questions.index')->with('questions' ,$questions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        if(!Auth::check()) {
            return redirect('questions')->with('error', 'Access denied');
        }
        $chapitres = chapitre::get();
        return view('questions.create',compact('chapitres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'type' => 'required',
            'chapitre_id' => 'required',
            'score'=>'required'
        ]);
        $question = new Question;
        $question->question = $request->input('question');
        $question->type = $request->input('type');
        $question->chapitre_id = $request->input('chapitre_id');
        $question->score = $request->input('score');
        $question->save();
        $questions   = Question::paginate(10);
        return view ('questions.index')->with('questions' ,$questions)->with('success', 'Questions added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $question = Question::find($id);
        return view('questions.show',compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function edit($id)
    {
        $question = Question::find($id);

        if (!isset($question )){
            return view('questions.index')->with('error', 'Question not found');
        }

        if(!Auth::check()) {
            return redirect('questions')->with('error', 'Access denied');
        }
        $chapitres = chapitre::get();
        return view('questions.edit',compact('question','chapitres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'question' => 'required',
            'type' => 'required',
            'chapitre_id' => 'required',
            'score'=>'required'
        ]);
        $question = Question::find($id);

        $question->question = $request->input('question');
        $question->type = $request->input('type');
        $question->chapitre_id = $request->input('chapitre_id');
        $question->score = $request->input('score');
        $question->save();
        $questions   = Question::paginate(10);
        return view ('questions.index')->with('questions' ,$questions)->with('success', 'Questions modifié avec success');

//        return view('questions.index')->with('success', 'Question modifié avec success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $question= Question::find($id);
        if(!Auth::check()) {
            return view('questions.index')->with('error', 'Unauthorized page');
        }
        $question->delete();
        return view('questions.index')->with('success', 'Question Supprimé');
    }
}
