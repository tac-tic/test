<?php

namespace App\Http\Controllers;

use App\Domaine;
use App\Question;
use App\Reponse;
use App\chapitre;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChapitresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $chapitres = chapitre::paginate(10);
        return view ('chapitres.index')->with('chapitres' ,$chapitres);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {   if(!Auth::check()) {
        $chapitres = chapitre::paginate(10);
        return redirect('chapitres')->with('error', 'Access denied')->with('chapitres',$chapitres);
    }
        $domaines = Domaine::get();
        return view('chapitres.create',compact('domaines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom' => 'required',
            'domaine_id' => 'required',
            'description' => 'required'
        ]);
        $chapitre = new chapitre;
        $chapitre->nom = $request->input('nom');
        $chapitre->description = $request->input('description');
        $chapitre->domaine_id = $request->input('domaine_id');


        $chapitre->save();
        $chapitres = Chapitre::paginate(10);

        return view ('chapitres.index')->with('chapitres' ,$chapitres)->with('success', 'Chapitre added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($id)
    {
        $chapitre = Chapitre::find($id);
        return view('chapitres.show',compact('chapitre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $chapitre = Chapitre::find($id);

        if (!isset($chapitre )){
            return view('chapitres')->with('error', 'chapitre not found');
        }
        if(!Auth::check()){

            return redirect('chapitres')->with('error', 'Access denied');
        }
        $domaines = Domaine::get();

        return view('chapitres.edit',compact('chapitre','domaines'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom' => 'required',
            'domaine_id' => 'required',
            'description' => 'required'
        ]);
        $chapitre = Chapitre::find($id);
        $chapitre->nom = $request->input('nom');
        $chapitre->description = $request->input('description');
        $chapitre->domaine_id = $request->input('domaine_id');
        $chapitre->save();

        return view('chapitres')->with('success', 'Chapitre Modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function destroy($id)
    {
        $chapitre= Chapitre::find($id);

        if(!Auth::check()){
            return redirect('chapitres')->with('error', 'Unauthorized page');

        }


        $chapitre->delete();
        return view('chapitres')->with('success', 'Chapitre Supprimé');
    }


    public function calcul(Request $request)
    {
        if(count($request->input()) < 2)
        {
            dd('the request shouldent be empty');
        }


        $result =0;
        foreach ($request->input() as $input)
        {
            if (is_numeric($input))
            {
                $rep = Reponse::find($input);
                $question = Question::find($rep->question_id);
                $repexact = Reponse::where('question_id','=',$rep->question_id)->where('exact','=','1')->count();
                if($rep->exact)
                {
                    //dd($rep);
                    $result =$result +( $question->score /$repexact);
                }

            }
        }
        $max = 0;
        foreach( ($question->chapitre->questions) as $question )
        {
            $max = $max + $question->score;
        }
        //dd($question->chapitre->questions);

        $note = $result;
        return view('chapitres.results',compact('note','max'));
    }

}
