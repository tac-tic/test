<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function chapitre(){
        return $this->belongsTo('App\Chapitre');
    }
    public function reponses(){
        return $this->hasMany('App\Reponse');
    }
}
