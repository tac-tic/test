<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domaine extends Model
{
    protected $table = 'domaines';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function chapitres(){
        return $this->hasMany('App\Chapitre');
    }
}
