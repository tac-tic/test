<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapitre extends Model
{
    protected $table = 'chapitres';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function domaine(){
        return $this->belongsTo('App\Domaine');
    }
    public function questions(){
        return $this->hasMany('App\Question');
    }
}
