import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


function showQuestion(number)
{    const questions = $('.question');
    questions.hide();
    questions.eq(Number(number)-1).show();

}

$( document ).ready(function() {
    const questions = $('.question');
    const prev = $('.previous');
    const next = $('.next');
    const btnSubmit = $('.submit');
    var selectedQuestion = 1;

    prev.attr('disabled','true');
    questions.hide();
    showQuestion(selectedQuestion);

    next.on('click', (e) => {
        e.preventDefault(e);
        showQuestion(Number(selectedQuestion)+1);
        selectedQuestion++;
        if (Number(selectedQuestion) === questions.length){
            next.attr('disabled','true');
            btnSubmit.removeAttr('disabled');
        }
        prev.removeAttr('disabled');
    });

    prev.on('click', (e) => {
        e.preventDefault(e);

        showQuestion(Number(selectedQuestion)-1);
        if (selectedQuestion === 1){
            prev.attr('disabled','true');

        }
        next.removeAttr('disabled');
        btnSubmit.attr('disabled','true');
        selectedQuestion--;
    });
});
