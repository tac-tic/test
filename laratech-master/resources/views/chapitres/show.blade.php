@extends('layouts.app')

@section('content')
    <a href="/dashboard" class="btn btn-default">Retour</a>
    <h1>Test {{$chapitre->nom}}</h1>
    <br>
    <div>
        <form method="POST" action="/chapitres/getscore" accept-charset="UTF-8" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if (count($chapitre->questions))
                @foreach ($chapitre->questions as $key => $question)

                    @if (count($question->reponses))

                        <div class="question">
                            <h3>{{$key+1}}-{{$question->question}}</h3>
                            @foreach ($question->reponses as $reponse)
                                @if ($question->type == 'mono')
                                    <div class="form-check">
                                        <input class="form-check-input" value="{{$reponse->id}}" type="radio"
                                               name="{{$question->id}}" id="rep{{$reponse->id}}">
                                        <label class="form-check-label" for="rep{{$reponse->id}}">
                                            {{$reponse->rep}}
                                        </label>
                                    </div>
                                @elseif ($question->type == 'multiple')
                                    <div class="form-check">
                                        <input class="form-check-input" value="{{$reponse->id}}" type="checkbox"
                                               name="{{$reponse->id}}" id="rep{{$reponse->id}}">
                                        <label class="form-check-label" for="rep{{$reponse->id}}">
                                            {{$reponse->rep}}
                                        </label>
                                    </div>
                                @endif

                            @endforeach
                        </div>

                    @endif
                @endforeach
            @endif

            <button class="btn btn-primary previous">Previous</button>
            <button class="btn btn-primary next">Next</button>
            <button disabled type="submit" class="btn btn-primary submit">Submit</button>

        </form>
    </div>
    <hr>

@endsection
