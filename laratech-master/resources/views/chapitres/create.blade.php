@extends('layouts.app')

@section('content')
    <h1>ajouter chapitre</h1>
    <form method="POST" action="/chapitres" accept-charset="UTF-8" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleFormControlInput1">nom</label>
            <input type="text" class="form-control" name="nom" id="exampleFormControlInput1" placeholder="nom chapitre">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Description</label>
            <input type="textarea" class="form-control" name="description" id="exampleFormControlInput1" placeholder="Lorem ipsum dolor sit amet consectetur ?">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect2">domaine</label>
            <select  class="form-control" id="exampleFormControlSelect2" name="domaine_id">
                @foreach ($domaines as $domaine)
                    <option value="{{$domaine->id}}" >{{$domaine->nom}}</option>
                @endforeach

            </select>
        </div>



        <button type="submit" class="btn btn-primary">Submit</button>


    </form>


@endsection
