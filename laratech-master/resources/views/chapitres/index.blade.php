@extends('layouts.app')

@section('content')
    <h1>Chapitres</h1>
    @if(count($chapitres) > 0)
    <ul class="list-group">



        @foreach($chapitres as $chapitre)
        <li class="list-group-item d-flex justify-content-between align-items-center">
            <a href="/chapitres/{{$chapitre->id}}">{{$chapitre->nom}}</a>
            <span class="badge badge-primary badge-pill">{{count($chapitre->questions)}} questions</span>
        </li>
        @endforeach
    </ul>
        {{$chapitres->links()}}
    @else
        <p>No chapitre found</p>
    @endif
@endsection
