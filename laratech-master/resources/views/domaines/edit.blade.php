@extends('layouts.app')

@section('content')
    <h1>Modifier domaine</h1>
    <form method="POST" action="/domaines/{{$domaine->id}}" accept-charset="UTF-8" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="_method" type="hidden" value="PUT">
        <div class="form-group">
            <label for="exampleFormControlInput1">nom</label>
            <input type="text" class="form-control" name="nom" id="exampleFormControlInput1" value="{{$domaine->nom}}">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Description</label>
            <input type="textarea" class="form-control" name="description" id="exampleFormControlInput1" value="{{$domaine->description}}">
        </div>




        <button type="submit" class="btn btn-primary">Submit</button>


    </form>

@endsection
