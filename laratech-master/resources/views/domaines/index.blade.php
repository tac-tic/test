@extends('layouts.app')

@section('content')
    <h1>Domaines</h1>
    <a href="/domaines/create" class="btn btn-primary">Ajouter domaine</a>
    <a href="/chapitres/create" class="btn btn-primary">Ajouter chapitre</a>
    <h3>Gerer les domaines</h3>
    @if(count($domaines) > 0)
        <ul class="list-group">



            @foreach($domaines as $domaine)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <a href="/domaines/{{$domaine->id}}">{{$domaine->nom}}</a>
                    <span class="badge badge-primary badge-pill">{{count($domaine->chapitres)}} chapitre</span>
                </li>
            @endforeach
        </ul>
        {{$domaines->links()}}
    @else
        <p>No domain found</p>
    @endif
@endsection
