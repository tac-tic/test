@extends('layouts.app')

@section('content')
    <h1>Ajouter domaine</h1>
    <form method="POST" action="/domaines" accept-charset="UTF-8" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="exampleFormControlInput1">nom</label>
            <input type="text" class="form-control" name="nom" id="exampleFormControlInput1" placeholder="nom domaine">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Description</label>
            <input type="textarea" class="form-control" name="description" id="exampleFormControlInput1" placeholder="Lorem ipsum dolor sit amet consectetur ?">
        </div>




        <button type="submit" class="btn btn-primary">Submit</button>


    </form>


@endsection
