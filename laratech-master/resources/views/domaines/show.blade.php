@extends('layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <a href="/domaines" class="btn btn-default">Retour</a>
                    <h1>{{$domaine->nom}}</h1>
                    <br>
                    <div>
                        @if (count($domaine->chapitres))
                        @foreach ($domaine->chapitres as $chapitre)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <a href="/chapitres/{{$chapitre->id}}">{{$chapitre->nom}}</a>
                                    <span class="badge badge-primary badge-pill">{{count($chapitre->questions)}} questions</span>
                                </li>

                        @endforeach
                            @endif
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </main>
@endsection
